import fs from 'fs'
import fse from 'fs-extra/esm'

var data = await fse.readJson("data.json");
var csv = "X,Y,NOM\n";

for(var d in data["documents"]){
    var feature = data["documents"][d];
    csv+=feature["geo_point"]["lon"]+","+feature["geo_point"]["lat"]+",n°"+feature["title"].replace(",","").replace("Caméra ","")+"\n";
}

fse.outputFile("data.csv", csv);
console.log("Done!");