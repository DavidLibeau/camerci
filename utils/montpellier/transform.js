import fs from 'fs'
import fse from 'fs-extra/esm'

var data = await fse.readJson("data.json");
var csv = "X,Y,NOM\n";

for(var d in data["features"]){
    var feature = data["features"][d];
    csv+=feature["geometry"]["coordinates"][0]+","+feature["geometry"]["coordinates"][1]+",n°"+feature["properties"]["OBJECTID"]+" "+feature["properties"]["Nom"]+" "+feature["properties"]["Adresse"].replace(",","")+"\n";
}

fse.outputFile("data.csv", csv);
console.log("Done!");